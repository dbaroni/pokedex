import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import pokemonTable from '@/views/pokemonTable.vue'
import registerPage from '@/views/Register.vue'
import loginPage from '@/views/Login.vue'
import PageNotFound from '@/views/Error404.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/pokemonAddList/:id',
      name: 'pokemonAddList',
      component: pokemonTable
    },
    {
      path: '/register',
      name: 'register',
      component: registerPage
    },
    {
      path: '/login',
      name: 'login',
      component: loginPage
    },
    {
      path: '/:catchAll(.*)*',
      name: "PageNotFound",
      component: PageNotFound,
    },
  ]
})

export default router




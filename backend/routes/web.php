<?php
/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('/', function () use ($router) {
    return phpinfo();
});


$router->post('/register', 'PokeController@register'); 

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('login', 'AuthController@login');
    $router->post('logout', 'AuthController@logout');
    $router->post('refresh', 'AuthController@refresh');
    $router->post('user-profile', 'AuthController@me');
 });


/*
$router->group([], function () use ($router) {
    $router->post('/api/login','AuthController@login');
    $router->post('/api/logout','AuthController@logout');
    $router->post('/api/refresh','AuthController@refresh');
    $router->get('/api/me','AuthController@me');
});
*/

$router->group(['middleware'=>'auth'], function () use ($router) {
    $router->get('/pokemon', 'PokeController@pokemon');
    $router->get('/pokemon/{id}', 'PokeController@searchPokemon');
    $router->get('/requireBox/{id}', 'PokeController@getPokemonByBox'); 
    $router->post('/addPokemon', 'PokeController@addPokemonByBox'); 
    $router->post('/removePokemon', 'PokeController@removePokemon'); 
    $router->get('/requireUserId/{id}', 'PokeController@getUserId'); 
 });


/*
Route::group([

    'prefix' => 'api'

], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('user-profile', 'AuthController@me');

});

*/
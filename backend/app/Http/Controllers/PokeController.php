<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class PokeController extends Controller
{

    public function pokemon(Request $request)
    {
        $result = app('db')->select(
            "SELECT * FROM pokemon"
        );

        return $result;
    }

    public function register(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $password = Hash::make($password);

        $results = app('db')->insert("INSERT INTO users(username, password) VALUES('$username', '$password')");

        $result = app('db')->select(
            "SELECT id FROM users ORDER BY id  DESC LIMIT 1"
        );
        
        $utente_id =  $result['0']->id;

        
        


        for ($i = 0; $i < 4; $i++) {
            $nome = 'Box: ' . $i . ' Utente: ' . $utente_id;
            $res = app('db')->insert("INSERT INTO box VALUES(null,  $utente_id,  '$nome')");
        }
        

        // Return 201 created
        return new Response(null, 201);
   
    }



    public function searchPokemon(Request $request, $id) 
    {
        $query = "";

        if ($id != null) {
            $query = "SELECT * FROM pokemon WHERE pokemon.pok_id='$id'";
        } else {
            $query = "SELECT * FROM pokemon";
        }
        

        $result = app('db')->select($query);
        return $result;
    }

    public function getPokemonByBox(Request $request, $id) {

        $box = $id;

        $result = app('db')->select(
            "SELECT pokemon.pok_id, pokemon.pok_name, box.id FROM users 
            LEFT JOIN box ON users.id=box.id_utente
            LEFT JOIN box_pokemon ON box.id=box_pokemon.id_box
            LEFT JOIN pokemon ON box_pokemon.id_pokemon=pokemon.pok_id
            WHERE box.id='$box' "
        );

        return $result;

    }

    public function getUserId(Request $request, $id) {

        

        $result = app('db')->select(
            "SELECT id FROM box WHERE id_utente = $id"
        );

        return $result;

    }


    public function addPokemonByBox(Request $request) {

        $box = $request->input("box");
        $pok_id = $request->input("pok_id");
        
        //$box = intval($box);
        //$pok_id = intval($pok_id);
        
        $result = app('db')->select(
            "INSERT INTO box_pokemon values('$box', '$pok_id')"
        );
        

        return $result;
    }

    public function removePokemon(Request $request) {
        $pok_id = $request->input("pok_id");
        $box = $request->input("box");

        $result = app('db')->select(
            "DELETE FROM box_pokemon WHERE id_box='$box' AND id_pokemon='$pok_id' LIMIT 1"
        );
        return $result;
    }


}
